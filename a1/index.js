let fetchRes = fetch('https://jsonplaceholder.typicode.com/todos');


fetchRes.then(res => res.json())
.then(toDo => toDo.map(function(order){
    if(order.title != null){
        // let title = {"title": order.title};
        // return title;
        return {"title": order.title}
    }
}))
.then(titlesList => console.log(titlesList));

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then(res => res.json())
.then(toDo => console.log("Title: "+toDo.title + "Status: " + toDo.completed));

fetch('https://jsonplaceholder.typicode.com/todos', {

	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
		userId: 1000,
		id: 1000,
		title: "Added using POST",
        completed: "false"
	})

}).then((response) => response.json())
.then((json) => console.log(json))
.catch((error) => {
    console.error('Error: ', error)
});

fetch('https://jsonplaceholder.typicode.com/todos/1', {

	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
		userId: 1,
		id: 1000,
		title: "Updated using PUT",
        completed: "True"
	})

}).then((response) => response.json())
.then((json) => console.log(json))
.catch((error) => {
    console.error('Error: ', error)
});

fetch('https://jsonplaceholder.typicode.com/todos/3', {
	method: 'DELETE',
}).then((response) => response.json())
.then((json) => console.log("Deleted"+json))
.catch((error) => {
    console.error('Error: ', error)
});

fetch('https://jsonplaceholder.typicode.com/todos/1', {

	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},

	body: JSON.stringify({
		userId: 1,
		id: 1000,
		title: "Updated using PUT",
        completed: "True",
        description: "Updated data structure",
        dateCompleted: "Jan 12, 2022"
	})

}).then((response) => response.json())
.then((json) => console.log(json))
.catch((error) => {
    console.error('Error: ', error)
});